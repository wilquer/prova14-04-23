#!/bin/bash

#!/bin/bash

echo "Informações do hardware:"

echo "-----------------"
echo "CPU"
echo "-----------------"
lscpu
echo "-----------------"
echo "Memória"
echo "-----------------"
free -m
echo "-----------------"
echo "Armazenamento"
echo "-----------------"
df -h
echo "-----------------"
echo "Placa-mãe"
echo "-----------------"
sudo dmidecode -t baseboard
echo "-----------------"
echo "Placa de vídeo"
echo "-----------------"
lspci | grep -i vga
echo "-----------------"
echo "Periféricos USB"
echo "-----------------"
lsusb

