#!/bin/bash

echo "Algumas variáveis automáticas do Bash:"

echo "\$BASH: $BASH"
echo "\$BASH_VERSION: $BASH_VERSION"
echo "\$BASH_VERSINFO: ${BASH_VERSINFO[*]}"
echo "\$BASH_COMPLETION: $BASH_COMPLETION"
echo "\$BASH_LINENO: ${BASH_LINENO[*]}"
echo "\$BASH_SOURCE: ${BASH_SOURCE[*]}"
echo "\$EUID: $EUID"
echo "\$PPID: $PPID"
echo "\$SECONDS: $SECONDS"
echo "\$SHELL: $SHELL"


