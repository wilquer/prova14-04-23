#!/bin/bash

echo Há pensamentos que são orações. Há momentos nos quais, seja qual for a posição do corpo, a alma está de joelhos.
sleep 2


echo Há momentos infelizes em que a solidão e o silêncio se tornam meios de liberdade.
sleep 2


echo A pessoa certa é a que está ao seu lado nos momentos incertos.
sleep 2


echo É nos momentos de decisão que o seu destino é traçado.
sleep 2


echo O caráter pode se manifestar nos grandes momentos, mas ele é formado nos pequenos.
sleep 2


echo Algumas pessoas têm longos momentos de silêncio, que tornam a sua convivência deliciosa.
sleep 2


echo Nem me seduz o vício, nem adoro a virtude.
sleep 2


echo Nossas vidas são definidas por momentos. Principalmente aqueles que nos pegam de surpresa
sleep 2


echo Existem momentos em que ainda é preciso correr riscos, dar passos loucos.
sleep 2


echo E se tudo fosse para sempre, você saberia dar valor aos bons momentos?
sleep 2
